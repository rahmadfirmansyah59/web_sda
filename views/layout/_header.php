<!--<div class="top-bar">
    <div class="top-bar-inner">
        <ul class="top-bar-info">
            <?php
            if (!empty($setting->email)) {
                echo '<li><a href="#">Email : ' . $setting->email . '</a></li>';
            }
            if (!empty($setting->telepon)) {
                echo '<li><a href="#">Telepon : ' . $setting->telepon . '</a></li>';
            }
            ?>
        </ul>
    </div>
</div>-->
<header class="header" id="page-header">
    <div class="header-inner">
        <div class="logo">
            <a href="<?=site_url()?>" title="<?=$setting->nama?>"><img src="<?=site_url()?>app/img/system/logo.png"></a>
        </div>
        <div class="logo-sm">
            <a href="<?=site_url()?>" title="<?=$setting->nama?>"><img src="<?=site_url()?>app/img/system/Logo-3G.jpg"></a>
        </div>
        <div id="btn-navbar">
            <button type="button" id="menu-mobile" class="navbar-toggle" data-toggle="collapse" data-target="#navbar" aria-expanded="true" aria-controls="navbar">
                <span class="glyphicon glyphicon-align-justify"></span>
            </button>
        </div>
        <div class="navi">
            <ul class="navi-level-1">
                <li>
                    <a href="<?= url('profil') ?>" class="dark-text">Profil</a>
                </li>
                <li>
                    <a href="#" class="dark-text has-sub">Berita</a>
                    <ul class="navi-level-2">
                        <li>
                            <a href="<?= url('c/agenda') ?>">Agenda</a>
                        </li>
                        <li>
                            <a href="<?= url('c/kegiatan') ?>">Kegiatan</a>
                        </li>
                        <li>
                            <a href="<?= url('c/teknologi') ?>">Teknologi</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#" class="dark-text has-sub">Produk</a>
                    <ul class="navi-level-2">
                        <li>
                            <a href="<?= url('c/jasa') ?>">Jasa</a>
                        </li>
                        <li>
                            <a href="<?= url('c/barang') ?>">Barang</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="<?= url('kunjungan') ?>" class="dark-text">Kunjungan</a>
                </li>
                <li>
                    <a href="<?= url('gallery') ?>" class="dark-text">Gallery</a>
                </li>
                <li>
                    <a href="<?= url('kontak') ?>" class="dark-text">Kontak Kami</a>
                </li>
                <li>
                    <a href="#" class="btn-search-navi color-theme border-color-theme bg-hover-theme">
                        <i class="glyphicon glyphicon-search search" aria-hidden="true"></i>
                    </a>
                    <div class="search-popup">
                        <form class="form-search-navi" action="<?= url("search") ?>">
                            <div class="input-group">
                                <input class="form-control" name="q" style="width: 250px;" placeholder="Masukkan Kata Kunci" type="text">
                            </div>
                        </form>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</header>


